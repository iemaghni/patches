#!/bin/sh
set -e

test -d .git || git init

while read -r f; do
	test -n "$f" && git add -f "./${f}"
done <<'EOF'
services/audio/audio_sandbox_hook_linux.cc

chrome/browser/ash/arc/enterprise/cert_store/arc_cert_installer.cc
chrome/browser/ash/arc/net/cert_manager_impl.cc
chrome/browser/ash/login/easy_unlock/easy_unlock_tpm_key_manager.cc
chrome/browser/ash/login/easy_unlock/easy_unlock_tpm_key_manager_unittest.cc
chrome/browser/ash/net/client_cert_store_ash.cc
chrome/browser/ash/ownership/owner_settings_service_ash.cc
chrome/browser/ash/platform_keys/chaps_util.h
chrome/browser/ash/platform_keys/chaps_util_impl.cc
chrome/browser/ash/platform_keys/platform_keys_service_nss.cc
chrome/browser/ash/scoped_test_system_nss_key_slot_mixin.h
chrome/browser/ash/system_token_cert_db_initializer.cc
chrome/browser/extensions/api/enterprise_platform_keys/enterprise_platform_keys_apitest_nss.cc
chrome/browser/extensions/api/platform_keys/platform_keys_apitest_nss.cc
chrome/browser/net/nss_service_linux.cc
chrome/browser/ui/crypto_module_password_dialog_nss.cc
chrome/common/net/x509_certificate_model_nss.cc
chrome/third_party/mozilla_security_manager/nsNSSCertHelper.cpp
chrome/third_party/mozilla_security_manager/nsNSSCertHelper.h
chrome/third_party/mozilla_security_manager/nsNSSCertificate.cpp
chrome/third_party/mozilla_security_manager/nsNSSCertificate.h
chrome/utility/importer/nss_decryptor.cc
chrome/utility/importer/nss_decryptor_system_nss.cc
chrome/utility/importer/nss_decryptor_system_nss.h
crypto/chaps_support.cc
crypto/chaps_support.h
crypto/nss_key_util.cc
crypto/nss_key_util.h
crypto/nss_key_util_unittest.cc
crypto/nss_util.cc
crypto/nss_util_chromeos.cc
crypto/nss_util_internal.h
crypto/scoped_nss_types.h
crypto/scoped_test_nss_db.cc
crypto/scoped_test_system_nss_key_slot.h
net/cert/internal/system_trust_store.cc
net/cert/internal/system_trust_store_nss_unittest.cc
net/cert/internal/trust_store_nss.cc
net/cert/internal/trust_store_nss.h
net/cert/internal/trust_store_nss_unittest.cc
net/cert/known_roots_nss.cc
net/cert/nss_cert_database.cc
net/cert/nss_cert_database_chromeos.cc
net/cert/nss_cert_database_unittest.cc
net/cert/nss_profile_filter_chromeos_unittest.cc
net/cert/scoped_nss_types.h
net/cert/x509_util_nss.cc
net/ssl/client_cert_store_nss.cc
net/ssl/client_cert_store_nss_unittest.cc
net/ssl/ssl_platform_key_nss.cc
net/test/cert_test_util.h
net/test/cert_test_util_nss.cc
net/third_party/mozilla_security_manager/nsNSSCertificateDB.cpp
net/third_party/mozilla_security_manager/nsPKCS12Blob.cpp
net/third_party/nss/ssl/cmpcert.cc
net/third_party/nss/ssl/cmpcert.h

third_party/blink/renderer/bindings/scripts/bind_gen/style_format.py
third_party/closure_compiler/compiler.py
third_party/node/node.py

content/browser/sandbox_ipc_linux.cc
content/common/font_list_fontconfig.cc
media/capture/video/linux/v4l2_capture_delegate.cc
media/capture/video/linux/v4l2_capture_delegate_unittest.cc
media/capture/video/linux/v4l2_capture_device.h
media/capture/video/linux/v4l2_capture_device_impl.cc
media/capture/video/linux/v4l2_capture_device_impl.h
EOF

git commit -m 'Initial commit'
# git add -u .
